package com.msb.client.consul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class ConsulClientConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsulClientConsumerApplication.class, args);
	}

	@Autowired
	RestTemplate restTemplate;


	@GetMapping("/consumer")
	public String consumer(){
		return restTemplate.getForObject("http://consul-client-provider/provider",String.class) + " consumer";
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate(){
		return new RestTemplate();
	}

}