package com.msb.client.consul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ConsulClientProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsulClientProviderApplication.class, args);
	}

	@GetMapping("/provider")
	public String provider(){

		return "provider";
	}

}