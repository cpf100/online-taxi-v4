package com.msb.onlinetaxi.client;

//import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
//@EnableApolloConfig
@MapperScan("com.msb.onlinetaxi.client.mapper")
public class OnlineTaxiApolloClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTaxiApolloClientApplication.class, args);
	}


}
