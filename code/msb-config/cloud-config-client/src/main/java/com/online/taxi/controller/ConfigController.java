package com.online.taxi.controller;

//import com.online.taxi.service.ConfigService;
//import com.online.taxi.component.GitConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**测试获取属性的类
 * @author yueyi2019
 */
@RefreshScope
@RestController
@RequestMapping("/config")
public class ConfigController {


	
	@Value("${name}")
	private String name;



	@GetMapping("/env0")
	public String env0() {
		return "远程：name:"+name;
	}

	@GetMapping("/env-config")
	public String envConfig(@Value("${config}") String env) {
		return "远程：env:"+ env;
	}



}
