package com.example.apidemo.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;

import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * @author 马士兵教育:chaopengfei
 * @date 2020/12/17
 */
public class NacosConfig {

	public static void main(String[] args) throws NacosException, InterruptedException {
		String serverAddr = "127.0.0.1:8848";
		String namespace = "c9fe2a3b-6457-49fa-b693-05cbe8067d1a";
//		String dataId = "test-data-id.yaml";
		String dataId = "cpf.yaml";
		String groupId = "DEFAULT_GROUP";

		Properties properties = new Properties();
		properties.put("serverAddr",serverAddr);
		properties.put("namespace",namespace);

		ConfigService configService = NacosFactory.createConfigService(properties);

		String config = configService.getConfig(dataId, groupId, 5000);
		System.out.println(config);

		configService.addListener(dataId, groupId, new Listener() {
			@Override
			public Executor getExecutor() {
				return null;
			}

			@Override
			public void receiveConfigInfo(String s) {
				System.out.println("监听:"+s);
			}
		});

		while (true){
			TimeUnit.MINUTES.sleep(1);
		}

	}
}
