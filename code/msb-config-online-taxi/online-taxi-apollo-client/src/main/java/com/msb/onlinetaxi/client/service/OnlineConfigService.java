package com.msb.onlinetaxi.client.service;


import com.msb.onlinetaxi.client.entity.OnlineConfig;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 晁鹏飞
 * @since 2021-01-21
 */
public interface OnlineConfigService {

	void insert(OnlineConfig onlineConfig);
}
