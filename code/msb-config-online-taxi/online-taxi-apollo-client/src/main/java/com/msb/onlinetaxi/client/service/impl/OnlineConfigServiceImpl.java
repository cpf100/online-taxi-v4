package com.msb.onlinetaxi.client.service.impl;

import com.msb.onlinetaxi.client.entity.OnlineConfig;
import com.msb.onlinetaxi.client.mapper.OnlineConfigMapper;
import com.msb.onlinetaxi.client.service.OnlineConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 晁鹏飞
 * @since 2021-01-21
 */
@Service
public class OnlineConfigServiceImpl implements OnlineConfigService {

	@Autowired
	OnlineConfigMapper onlineConfigMapper;

	@Override
	public void insert(OnlineConfig onlineConfig) {

		onlineConfigMapper.insert(onlineConfig);
	}
}
