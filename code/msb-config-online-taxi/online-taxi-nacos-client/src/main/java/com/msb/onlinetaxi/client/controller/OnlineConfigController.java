package com.msb.onlinetaxi.client.controller;



import com.msb.onlinetaxi.client.entity.OnlineConfig;
import com.msb.onlinetaxi.client.mapper.OnlineConfigMapper;
import com.msb.onlinetaxi.client.service.OnlineConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 晁鹏飞
 * @since 2021-01-21
 */
@RestController
@RequestMapping("/online-config")
public class OnlineConfigController {

	@Value("${config-value}")
	String configValue;

	@Autowired
	OnlineConfigService onlineConfigService;

	@GetMapping("/config")
	public String config(){
		return configValue;
	}

	@GetMapping("/save")
	public String insert(){
		OnlineConfig onlineConfig = new OnlineConfig();
		onlineConfig.setName("name1");
		onlineConfig.setAge(4);
		onlineConfigService.insert(onlineConfig);
		return "success";
	}

	@Autowired
	OnlineConfigMapper onlineConfigMapper;

	@GetMapping("/query")
	public OnlineConfig query(){
		return onlineConfigMapper.selectByName("name1");
	}
}

